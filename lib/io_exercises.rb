# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  comp_gen_num = rand(1..100)
  guess_count = 0

  while true
    puts "Guess a number"
    guess = gets.chomp.to_i
    guess_count += 1
    if guess > comp_gen_num
      puts "Your guess, #{guess}, was too high."
    elsif guess < comp_gen_num
      puts "Your guess, #{guess}, was too low."
    else
      puts "You guessed correct! The number was #{comp_gen_num}."
      puts "Your guess count was #{guess_count}."
      break
    end
  end
end

def file_shuffler(file_name)
  shuffled_file = File.open("#{file_name}-shuffled.txt", "w")
  File.open("#{file_name}.txt", "w") do |f|
    File.readlines(f).each do |line|
      shuffled_file.puts line.shuffle
    end
  end
  shuffled_file.close
end
